package com.ynavizovskyi.rates.domain

data class RateItem(val currencyCode: String, var value: Double, val currencyName: String?)