package com.ynavizovskyi.rates.domain.repository

import com.ynavizovskyi.rates.domain.RateItem
import com.ynavizovskyi.rates.domain.remote.Api
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class RatesRepositoryImpl @Inject constructor(val api: Api) : RatesRepository {

    override fun getRates(baseCurrency: String): Single<List<RateItem>> {
        return api.getUser(baseCurrency)
            .flatMap { Observable.fromIterable(it.rates.entries).map { RateItem(it.key, it.value, null) }.toList() }
    }

}