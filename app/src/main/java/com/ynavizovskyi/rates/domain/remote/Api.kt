package com.ynavizovskyi.rates.domain.remote

import com.ynavizovskyi.rates.domain.remote.entities.RatesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface Api {

    @GET("latest")
    @Headers("Content-Type: application/json", "Accept-Language: uk")
    fun getUser(@Query("base") baseCurrency: String): Single<RatesResponse>

}