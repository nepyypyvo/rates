package com.ynavizovskyi.rates.domain.repository

import com.ynavizovskyi.rates.domain.RateItem
import io.reactivex.Single

interface RatesRepository {

    fun getRates(baseCurrency: String): Single<List<RateItem>>

}