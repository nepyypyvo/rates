package com.ynavizovskyi.rates.domain.remote.entities

data class RatesResponse(val rates: Map<String, Double>)