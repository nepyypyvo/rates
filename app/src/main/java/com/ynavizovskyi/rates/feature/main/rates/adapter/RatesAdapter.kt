package com.ynavizovskyi.rates.feature.main.rates.adapter

import android.content.Context
import android.view.ViewGroup
import com.ynavizovskyi.rates.R
import com.ynavizovskyi.rates.base.adapter.BaseRecyclerViewAdapter
import com.ynavizovskyi.rates.domain.RateItem

class RatesAdapter(context: Context?) :
    BaseRecyclerViewAdapter<RateItem, RatesAdapterPresenter, RatesAdapter, RatesViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesViewHolder {
        return RatesViewHolder(layoutInflater.inflate(R.layout.lineitem_rate, parent, false))
    }

    override fun createPresenter(): RatesAdapterPresenter = RatesAdapterPresenter()

    override fun getAdapter(): RatesAdapter = this

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}