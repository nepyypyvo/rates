package com.ynavizovskyi.rates.feature.main.activity

import com.ynavizovskyi.rates.application.di.scope.FragmentScope
import com.ynavizovskyi.rates.feature.main.rates.RatesFragment
import com.ynavizovskyi.rates.feature.main.rates.RatesFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBuilder {

    @ContributesAndroidInjector(modules = arrayOf(RatesFragmentModule::class))
    @FragmentScope
    internal abstract fun bindFeedFragment(): RatesFragment
}