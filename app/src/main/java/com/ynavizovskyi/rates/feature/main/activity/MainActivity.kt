package com.ynavizovskyi.rates.feature.main.activity

import com.ynavizovskyi.rates.R
import com.ynavizovskyi.rates.base.activity.BaseActivity

class MainActivity : BaseActivity<MainActivityContract.Presenter>(), MainActivityContract.View {

    override fun getLayout(): Int {
        return R.layout.activity_main
    }
}
