package com.ynavizovskyi.rates.feature.main.rates

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.ynavizovskyi.rates.R
import com.ynavizovskyi.rates.base.adapter.ItemEvent
import com.ynavizovskyi.rates.base.fragment.BaseFragment
import com.ynavizovskyi.rates.domain.RateItem
import com.ynavizovskyi.rates.feature.main.rates.adapter.RatesAdapter
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_rates.*

class RatesFragment : BaseFragment<RatesFragmentContract.Presenter>(), RatesFragmentContract.View {

    override fun getLayout(): Int = R.layout.fragment_rates

    private var adapter: RatesAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = RatesAdapter(activity)
        rv_rates.layoutManager = LinearLayoutManager(activity)
        rv_rates.adapter = this.adapter
    }

    override fun displayLoadingProgress() {
        ll_progress.visibility = View.VISIBLE
        tv_failed.visibility = View.GONE
    }

    override fun displayLoadingFailure() {
        tv_failed.visibility = View.VISIBLE
        ll_progress.visibility = View.GONE
    }

    override fun displayRates(rates: List<RateItem>) {
        ll_progress.visibility = View.GONE
        tv_failed.visibility = View.GONE
        rv_rates.visibility = View.VISIBLE
        rv_rates.post(Runnable { adapter?.setData(rates) })

    }

    override fun getRateItemActionObservable(): Observable<ItemEvent<RateItem>> =
        adapter?.getOnItemActionObservable() ?: Observable.empty()
}