package com.ynavizovskyi.rates.feature.main.activity

import com.ynavizovskyi.rates.application.di.scope.ActivityScope
import com.ynavizovskyi.rates.feature.main.activity.navigator.MainNavigator
import com.ynavizovskyi.rates.feature.main.activity.navigator.MainNavigatorImpl
import dagger.Binds
import dagger.Module

@Module
abstract class MainModule {

    @Binds
    @ActivityScope
    abstract fun provideView(activity: MainActivity): MainActivityContract.View

    @Binds
    @ActivityScope
    abstract fun provideNavigator(navigator: MainNavigatorImpl): MainNavigator

    @Binds
    @ActivityScope
    abstract fun bindPresenter(presenter: MainActivityPresenter): MainActivityContract.Presenter

}