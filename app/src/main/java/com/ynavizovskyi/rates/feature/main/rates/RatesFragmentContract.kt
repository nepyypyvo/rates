package com.ynavizovskyi.rates.feature.main.rates

import com.ynavizovskyi.rates.base.adapter.ItemEvent
import com.ynavizovskyi.rates.base.fragment.BaseFragmentContract
import com.ynavizovskyi.rates.domain.RateItem
import io.reactivex.Observable

interface RatesFragmentContract {

    interface View: BaseFragmentContract.View{

        fun displayLoadingProgress()

        fun displayLoadingFailure()

        fun displayRates(rates: List<RateItem>)

        fun getRateItemActionObservable(): Observable<ItemEvent<RateItem>>

    }

    interface Presenter: BaseFragmentContract.Presenter

}