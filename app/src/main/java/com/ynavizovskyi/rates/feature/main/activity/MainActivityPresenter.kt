package com.ynavizovskyi.rates.feature.main.activity

import com.ynavizovskyi.rates.base.activity.BaseActivityPresenter
import com.ynavizovskyi.rates.feature.main.activity.navigator.MainNavigator
import javax.inject.Inject

class MainActivityPresenter @Inject constructor(view: MainActivityContract.View, navigator: MainNavigator) :
    BaseActivityPresenter<MainActivityContract.View, MainNavigator>(view, navigator),
    MainActivityContract.Presenter {

    override fun onCreate() {
        super.onCreate()
        navigator.presentRatesScreen()
    }
}