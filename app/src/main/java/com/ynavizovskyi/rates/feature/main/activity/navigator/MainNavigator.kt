package com.ynavizovskyi.rates.feature.main.activity.navigator

import com.ynavizovskyi.rates.base.navigator.BaseNavigator

interface MainNavigator : BaseNavigator {

    fun presentRatesScreen()

}