package com.ynavizovskyi.rates.feature.main.activity

import com.ynavizovskyi.rates.base.activity.BaseActivityContract

interface MainActivityContract {

    interface View: BaseActivityContract.View

    interface Presenter: BaseActivityContract.Presenter
}