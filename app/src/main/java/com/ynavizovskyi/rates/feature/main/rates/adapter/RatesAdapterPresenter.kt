package com.ynavizovskyi.rates.feature.main.rates.adapter

import com.ynavizovskyi.rates.base.adapter.BaseRecyclerViewPresenter
import com.ynavizovskyi.rates.domain.RateItem

class RatesAdapterPresenter : BaseRecyclerViewPresenter<RateItem, RatesAdapter, RatesViewHolder>() {

    override var areItemsTheSame: (RateItem, RateItem) -> Boolean = { old, new -> old.currencyCode == new.currencyCode }

    override var getPayloadDifference: (RateItem, RateItem) -> Any? = { old, new -> new.value }

}