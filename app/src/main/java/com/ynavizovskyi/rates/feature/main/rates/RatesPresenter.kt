package com.ynavizovskyi.rates.feature.main.rates

import com.ynavizovskyi.rates.application.round
import com.ynavizovskyi.rates.application.roundDown
import com.ynavizovskyi.rates.base.adapter.ItemEventType
import com.ynavizovskyi.rates.base.fragment.BaseFragmentPresenter
import com.ynavizovskyi.rates.domain.RateItem
import com.ynavizovskyi.rates.domain.repository.RatesRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RatesPresenter @Inject constructor(view: RatesFragmentContract.View, private val repository: RatesRepository) :
    BaseFragmentPresenter<RatesFragmentContract.View>(view), RatesFragmentContract.Presenter {

    private val DECIMAL_PRECISION = 2
    private val UPDATE_INTERVAL_MILLS = 1000L

    private val eurItem = RateItem("EUR", 1.0, null)
    private var lastEditedItem = eurItem
    private var originalItems: MutableList<RateItem> = mutableListOf()
    private var displayedItems: MutableList<RateItem> = mutableListOf()

    override fun onViewCreated() {
        super.onViewCreated()
        loadRates()
    }

    override fun onResume() {
        super.onResume()
        subscribeToItemEvents()
    }

    private fun loadRates() {
        view.displayLoadingProgress()
        repository.getRates("EUR")
            .map {
                var mutableList = it.toMutableList()
                mutableList.add(0, eurItem)
                originalItems = mutableList
                mutableList
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { onDataSetUpdated() }
            .doOnError { if (originalItems.isEmpty()) view.displayLoadingFailure() }
            .zipWith(Single.timer(UPDATE_INTERVAL_MILLS, TimeUnit.MILLISECONDS),
                BiFunction { item: Any, _: Long -> item })
            .repeat()
            .toObservable()
//            .compose(storeObservableDisposable())
            .subscribe()
    }

    private fun subscribeToItemEvents() {
        view.getRateItemActionObservable()
            .filter { it.type == ItemEventType.UPDATE || it.type == ItemEventType.FOCUSED }
            .compose(storeObservableDisposable())
            .subscribe { itemEvent ->
                lastEditedItem = itemEvent.data
                //update edited item in order to avoid the need to redraw it in recycler view
                displayedItems.replaceAll { if (it.currencyCode == lastEditedItem.currencyCode) lastEditedItem else it }

                if (itemEvent.type == ItemEventType.UPDATE)
                    onDataSetUpdated()
            }
    }

    private fun onDataSetUpdated() {
        val lastEditedItemOriginal =
            originalItems.find { rateItem -> rateItem.currencyCode == lastEditedItem.currencyCode }

        val multiplier = lastEditedItem.value.div(lastEditedItemOriginal?.value ?: 1.0).round(DECIMAL_PRECISION)

        displayedItems = originalItems.map {
            if (it.currencyCode == lastEditedItem?.currencyCode) lastEditedItem else it?.copy(
                value = (it.value * multiplier).roundDown(DECIMAL_PRECISION)
            )
        }.toMutableList()
        view.displayRates(displayedItems)

    }

}
