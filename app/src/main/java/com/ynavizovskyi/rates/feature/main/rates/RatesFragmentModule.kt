package com.ynavizovskyi.rates.feature.main.rates

import com.ynavizovskyi.rates.application.di.scope.FragmentScope
import dagger.Binds
import dagger.Module

@Module
abstract class RatesFragmentModule {

    @Binds
    @FragmentScope
    abstract fun provideView(fragment: RatesFragment): RatesFragmentContract.View

    @Binds
    @FragmentScope
    abstract fun bindUserProfileFragmentPresenter(presenter: RatesPresenter): RatesFragmentContract.Presenter

}