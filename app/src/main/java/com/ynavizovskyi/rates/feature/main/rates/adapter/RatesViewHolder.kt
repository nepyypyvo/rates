package com.ynavizovskyi.rates.feature.main.rates.adapter

import android.text.InputFilter
import android.text.Spanned
import android.text.TextWatcher
import android.view.View
import com.ynavizovskyi.rates.R
import com.ynavizovskyi.rates.application.afterTextChanged
import com.ynavizovskyi.rates.base.adapter.BaseRecyclerViewHolder
import com.ynavizovskyi.rates.base.adapter.ItemEvent
import com.ynavizovskyi.rates.base.adapter.ItemEventType
import com.ynavizovskyi.rates.domain.RateItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.lineitem_rate.view.*
import java.util.regex.Pattern


class RatesViewHolder(val view: View) : BaseRecyclerViewHolder<RateItem>(view) {

    private val currencyCodeTextView = view.tv_currency_code
    private val valueTextView = view.tv_value
    private val amountEditText = view.et_amount
    private val flagImageView = view.iv_flag
    private var textWatcher: TextWatcher? = null

    override fun bind(
        itemData: RateItem,
        publishSubject: PublishSubject<ItemEvent<RateItem>>
    ) {
        super.bind(itemData, publishSubject)
        currencyCodeTextView.text = itemData.currencyCode
        valueTextView.text = itemData.currencyCode
        amountEditText.setText(itemData.value.toString())

        textWatcher = amountEditText.afterTextChanged { s ->
            if (amountEditText.isFocused) {
                publishSubject.onNext(ItemEvent(itemData.copy(
                    value = try { s.toDouble() } catch (e: NumberFormatException) { 0.0 }),
                    ItemEventType.UPDATE))
            }
        }

        amountEditText.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus){
                publishSubject.onNext(ItemEvent(itemData.copy(
                    value = try { amountEditText.text.toString().toDouble() } catch (e: NumberFormatException) { 0.0 }),
                    ItemEventType.FOCUSED))
            }
        }

        amountEditText.filters = arrayOf(Filter(6, 2))

        val id = view.resources.getIdentifier("ic_" + itemData.currencyCode.toLowerCase(), "drawable", view.context.packageName)
        flagImageView.setImageResource(if (id == 0) R.drawable.ic_undef else id)
    }

    override fun update(payload: MutableList<Any>) {
        amountEditText.setText((payload[0] as Double).toString())
    }

    override fun unbind() {
        amountEditText.removeTextChangedListener(textWatcher)
        textWatcher = null
    }

    inner class Filter(beforeDecimal: Int, afterDecimal: Int): InputFilter{

        private val pattern: Pattern = Pattern.compile( "^\\d{0,$beforeDecimal}([\\.,](\\d{0,$afterDecimal})?)?$")


        override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
            val newString = (dest.toString().substring(0, dstart) + source.toString().substring(start, end)
                    + dest.toString().substring(dend, dest.toString().length))

            val matcher = pattern.matcher(newString)
            return if (!matcher.matches() && source?.length == 1) {
                ""
            } else null
        }
    }

}