package com.ynavizovskyi.rates.feature.main.activity.navigator

import com.ynavizovskyi.rates.base.navigator.BaseNavigatorImpl
import com.ynavizovskyi.rates.feature.main.activity.MainActivityContract
import com.ynavizovskyi.rates.feature.main.rates.RatesFragment
import javax.inject.Inject

class MainNavigatorImpl @Inject constructor(activity: MainActivityContract.View) : BaseNavigatorImpl(activity),
    MainNavigator {

    val RATES_SCREEN_TAG = "rates_screen"
    val RATES_SCREEN_TITLE = "Rates"

    override fun presentRatesScreen() {
        switchFragment(RatesFragment(), RATES_SCREEN_TAG, RATES_SCREEN_TITLE)
    }
}