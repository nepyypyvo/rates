package com.ynavizovskyi.rates.application.di

import com.ynavizovskyi.rates.application.di.scope.ActivityScope
import com.ynavizovskyi.rates.feature.main.activity.MainActivity
import com.ynavizovskyi.rates.feature.main.activity.MainFragmentBuilder
import com.ynavizovskyi.rates.feature.main.activity.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(MainModule::class, MainFragmentBuilder::class))
    @ActivityScope
    internal abstract fun bindLoginActivity(): MainActivity

}