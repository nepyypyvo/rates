package com.ynavizovskyi.rates.application.di

import com.ynavizovskyi.rates.application.RatesApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidSupportInjectionModule::class, AppModule::class, RepositoryModule::class, ActivityBuilder::class))
interface RatesAppComponent {

    fun inject(application: RatesApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: RatesApp): Builder

        fun build(): RatesAppComponent
    }


}