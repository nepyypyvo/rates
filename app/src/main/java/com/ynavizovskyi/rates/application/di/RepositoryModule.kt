package com.ynavizovskyi.rates.application.di

import com.ynavizovskyi.rates.domain.repository.RatesRepository
import com.ynavizovskyi.rates.domain.repository.RatesRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindUserRepository(repository: RatesRepositoryImpl): RatesRepository

}