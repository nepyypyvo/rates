package com.ynavizovskyi.rates.base.adapter

import androidx.recyclerview.widget.DiffUtil
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


abstract class BaseRecyclerViewPresenter<DT,
        A : BaseRecyclerViewAdapter<DT, *, A, VH>,
        VH : BaseRecyclerViewHolder<DT>>
    : BaseAdapterContract.Presenter<DT, A, VH> {

    private lateinit var adapter: A
    private val subject: PublishSubject<ItemEvent<DT>> = PublishSubject.create()
    private var data: List<DT> = emptyList()

    override fun setAdapter(adapter: A) {
        this.adapter = adapter
    }

    override fun getAdapter(): A = adapter

    override fun setData(data: List<DT>) {
        val diffResult = DiffUtil.calculateDiff(DiffCallback(this.data, data, areItemsTheSame, getPayloadDifference))
        this.data = data
        diffResult.dispatchUpdatesTo(getAdapter())
    }

    override fun getData(): List<DT> = data

    override fun getItemCount(): Int = data.size


    override fun bindViewHolder(holder: VH, position: Int) {
        data[position]?.let { holder.bind(it, subject) }
    }

    override fun updateViewHolder(holder: VH, payloads: MutableList<Any>) = holder.update(payloads)

    override fun unbindViewHolder(holder: VH) = holder.unbind()

    override fun getOnItemActionObservable(): Observable<ItemEvent<DT>> = subject

    protected open var areItemsTheSame: (DT, DT) -> Boolean = { old, new -> false }

    protected open var getPayloadDifference: (DT, DT) -> Any? = { old, new -> null }

    internal class DiffCallback<DT>(
        val oldList: List<DT>,
        val newList: List<DT>,
        val equalFunc: (old: DT, new: DT) -> Boolean,
        val payloadFunc: (old: DT, new: DT) -> Any?
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            equalFunc(oldList[oldItemPosition], newList[newItemPosition])

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? =
            payloadFunc(oldList[oldItemPosition], newList[newItemPosition])
    }
}
