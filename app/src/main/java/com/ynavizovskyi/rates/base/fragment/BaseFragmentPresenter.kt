package com.ynavizovskyi.rates.base.fragment

import io.reactivex.ObservableTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragmentPresenter<out V : BaseFragmentContract.View>(protected val view: V) :
    BaseFragmentContract.Presenter {

    protected var activeDisposables: CompositeDisposable = CompositeDisposable()

    override fun onCreate() {

    }

    override fun onViewCreated() {

    }

    override fun onDestroyView() {

    }

    override fun onResume() {

    }

    override fun onPause() {
        activeDisposables.dispose()
    }

    protected fun storeDisposable(disposable: Disposable) {
        activeDisposables.add(disposable)
    }

    protected fun <T> storeObservableDisposable(): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream -> upstream.doOnSubscribe { this.storeDisposable(it) } }
    }

}