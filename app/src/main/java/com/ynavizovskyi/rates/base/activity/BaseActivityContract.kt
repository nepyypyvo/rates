package com.ynavizovskyi.rates.base.activity

interface BaseActivityContract {

    interface View

    interface Presenter{

        fun onCreate()

    }

}