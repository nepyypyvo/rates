package com.ynavizovskyi.rates.base.adapter

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

interface BaseAdapterContract {

    interface Adapter<DT, P : Presenter<DT, A, VH>, A : Adapter<DT, P, A, VH>, VH: ViewHolder<DT>> {

        fun createPresenter(): P

        fun getPresenter() : P

        fun getAdapter(): A

        fun setData(data: List<DT>)

        fun onDataSetChanged()

        fun onItemAdded(position: Int)

        fun onItemUpdated(position: Int)

        fun onItemRemoved(Position: Int)

        fun getOnItemActionObservable(): Observable<ItemEvent<DT>>

    }

    interface Presenter<DT, A : Adapter<DT, *, A, VH>, VH : ViewHolder<DT>> {

        fun setAdapter(adapter: A)

        fun getAdapter(): A

        fun setData(data: List<DT>)

        fun getData(): List<DT>

        fun getItemCount(): Int

        fun bindViewHolder(holder: VH, position: Int)

        fun updateViewHolder(holder: VH, payloads: MutableList<Any>)

        fun unbindViewHolder(holder: VH)

        fun getOnItemActionObservable(): Observable<ItemEvent<DT>>
    }

    interface ViewHolder<DT> {

        fun bind(itemData: DT, publishSubject: PublishSubject<ItemEvent<DT>>)

        fun update(payload: MutableList<Any>)

        fun unbind()

    }

}