package com.ynavizovskyi.rates.base.fragment

interface BaseFragmentContract {

    interface View {

    }

    interface Presenter {

        fun onCreate()

        fun onViewCreated()

        fun onDestroyView()

        fun onResume()

        fun onPause()

    }

}