package com.ynavizovskyi.rates.base.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity<T : BaseActivityContract.Presenter> : DaggerAppCompatActivity() {

    @Inject
    lateinit var presenter : T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        presenter.onCreate()
    }

    @LayoutRes
    abstract fun getLayout(): Int
}