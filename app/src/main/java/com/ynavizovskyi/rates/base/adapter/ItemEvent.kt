package com.ynavizovskyi.rates.base.adapter

data class ItemEvent<DT>(val data: DT, val type: ItemEventType)

enum class ItemEventType{
    CLICK, UPDATE, FOCUSED
}