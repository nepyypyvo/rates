package com.ynavizovskyi.rates.base.activity

import com.ynavizovskyi.rates.base.navigator.BaseNavigator

abstract class BaseActivityPresenter<out V : BaseActivityContract.View, N : BaseNavigator>(
    protected val view: V,
    protected val navigator: N
) : BaseActivityContract.Presenter {

    override fun onCreate() {

    }
}