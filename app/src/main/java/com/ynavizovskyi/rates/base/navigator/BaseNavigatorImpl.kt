package com.ynavizovskyi.rates.base.navigator

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.ynavizovskyi.rates.R
import com.ynavizovskyi.rates.base.activity.BaseActivityContract
import com.ynavizovskyi.rates.base.fragment.BaseFragment

abstract class BaseNavigatorImpl(private val activity: BaseActivityContract.View) : BaseNavigator {

    private val fragmentManager: FragmentManager = (activity as AppCompatActivity).supportFragmentManager
    //It is important that consuming activities have proper container name
    private val containerId = R.id.fragment_container

    protected fun switchFragment(fragment: BaseFragment<*>, tag: String, title: String) {
        (activity as AppCompatActivity).supportActionBar?.title = title
        fragmentManager.beginTransaction().replace(containerId, fragment).addToBackStack(tag).commit()

    }

}