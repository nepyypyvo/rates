package com.ynavizovskyi.rates.base.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.subjects.PublishSubject

abstract class BaseRecyclerViewHolder<DT>(view: View) : RecyclerView.ViewHolder(view), BaseAdapterContract.ViewHolder<DT> {

    override fun bind(itemData: DT, publishSubject: PublishSubject<ItemEvent<DT>>) {
        itemView.setOnClickListener { publishSubject.onNext(ItemEvent(itemData, ItemEventType.CLICK)) }
    }

    override fun update(payload: MutableList<Any>) {
        //Do nothing by default
    }

    override fun unbind() {
        //Do nothing by default
    }
}