package com.ynavizovskyi.rates.base.adapter

import android.content.Context
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable

abstract class BaseRecyclerViewAdapter<DT,
        P : BaseAdapterContract.Presenter<DT, A, VH>,
        A : BaseAdapterContract.Adapter<DT, P, A, VH>,
        VH : BaseRecyclerViewHolder<DT>>(context: Context?)
    : RecyclerView.Adapter<VH>(), BaseAdapterContract.Adapter<DT, P, A, VH> {

    protected val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    private val presenter: P = createPresenter()

    init {
        presenter.setAdapter(getAdapter())
    }

    override fun getPresenter(): P = presenter

    override fun setData(data: List<DT>) {
        presenter.setData(data)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        presenter.bindViewHolder(holder, position)
    }

    override fun onBindViewHolder(holder: VH, position: Int, payloads: MutableList<Any>) {
        if(payloads.size == 0){
            onBindViewHolder(holder, position)
        } else {
            presenter.updateViewHolder(holder, payloads)
        }
    }

    override fun onViewRecycled(holder: VH) = presenter.unbindViewHolder(holder)

    override fun getItemCount(): Int = presenter.getItemCount()

    override fun onDataSetChanged() = notifyDataSetChanged()

    override fun onItemAdded(position: Int) = notifyItemInserted(position)

    override fun onItemUpdated(position: Int) = notifyItemChanged(position)

    override fun onItemRemoved(position: Int) = notifyItemRemoved(position)

    override fun getOnItemActionObservable(): Observable<ItemEvent<DT>> = getPresenter().getOnItemActionObservable()
}